# Stage 1
FROM alpine:3.18 as sqlc

COPY --from=sqlc/sqlc:1.22.0 /workspace/sqlc /usr/bin/sqlc

WORKDIR /gen

COPY ./application/storage/ .

WORKDIR /gen/queries

RUN sqlc generate

# Stage 2
FROM golang:1.21-alpine3.18 as builder

# Copy code into the container. Note: copy to a dir instead of `.`, since $GOPATH may not contain a go.mod file
WORKDIR /build

COPY . .
COPY --from=sqlc /gen/queries/generated /build/application/storage/queries/generated

# Build the Go Files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .

# Stage 3
FROM alpine:3.18

# Add timezones
RUN apk add --no-cache tzdata

# Copy the bin from builder to root.
COPY --from=builder /build/server /
COPY assets /assets
COPY fonts /fonts

ENTRYPOINT ["./server", "serve"]

EXPOSE 80
