package application

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/rc"
)

type Municipality struct {
	FRP *rc.FRP
	WOZ *rc.RestCall
}

type Application struct {
	*http.Server
	municipalities    map[string]Municipality
	frag              *rc.RestCall
	db                *storage.Database
	ozbRunStatus      *Status
	finalizeRunStatus *Status
}

func New(listenAddress string, municipalities map[string]Municipality, frag *rc.RestCall, db *storage.Database) Application {
	return Application{
		Server: &http.Server{
			Addr: listenAddress,
		},
		municipalities: municipalities,
		frag:           frag,
		db:             db,
		ozbRunStatus: &Status{
			status: make(map[uuid.UUID]Work),
			lock:   sync.RWMutex{},
		},
		finalizeRunStatus: &Status{
			status: make(map[uuid.UUID]Work),
			lock:   sync.RWMutex{},
		},
	}
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.Logger)
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	r.Post("/runs", app.RunCreate)
	r.Get("/runs", app.RunList)
	r.Get("/runs/{id}", app.RunGet)
	r.Get("/runs/{id}/tariffs", app.RunGetTariffs)
	r.Get("/runs/{id}/status", app.RunStatus)
	r.Get("/runs/{id}/finalize", app.FinalizeTaxBill)

	r.Get("/tax-bills", app.TaxBillsList)
	r.Get("/tax-bills/{id}", app.TaxBillGet)
	r.Get("/tax-bills/{id}/assessments", app.TaxBillGetAssessment)
	r.Get("/tax-bills/{id}/download", app.TaxBillsDownload)
	r.Get("/tax-bills/{id}/status", app.TaxBillsStatus)
	r.Get("/owners/{id}/tax-bills", app.OwnerTaxBillsList)

	r.Get("/assessments", app.AssessmentsList)

	r.Get("/woz-objects/{id}/decisions", app.DecisionBasedOnWozObject)

	r.Route("/v3", func(r chi.Router) {
		r.Get("/health/", health)
		r.Get("/health/check/", health)
		r.Post("/financial_claims_information/", app.FinancialClaim)
	})

	app.Server.Handler = r
}

func health(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("."))
}

func writeError(w http.ResponseWriter, err error) {
	fmt.Printf("err: %v\n", err)
	http.Error(w, err.Error(), http.StatusInternalServerError)
}
