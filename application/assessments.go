package application

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/adapter"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/meta"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/pagination"
)

func (app *Application) AssessmentsList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*generated.DigilabDemoFbsAssessment

	switch md.IsASC() {
	case true:
		records, err = app.assessmentsListAsc(r.Context(), md)
	case false:
		records, err = app.assessmentsListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	objects := adapter.ToAssessments(records)

	response, err := pagination.Build(objects, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) assessmentsListAsc(ctx context.Context, md *meta.Data) ([]*generated.DigilabDemoFbsAssessment, error) {
	params := &generated.AssessmentListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.AssessmentListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	return records, nil
}

func (app *Application) assessmentsListDesc(ctx context.Context, md *meta.Data) ([]*generated.DigilabDemoFbsAssessment, error) {
	params := &generated.AssessmentListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.AssessmentListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	return records, nil
}

func (app *Application) TaxBillGetAssessment(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	records, err := app.db.Queries.AssessmentGetOnTaxBill(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	assessments := adapter.ToAssessments(records)

	if err := json.NewEncoder(w).Encode(assessments); err != nil {
		writeError(w, err)
		return
	}
}
