package application

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/adapter"
)

const (
	OIN = "00000001822948837000"
)

type FinancialClaimBody struct {
	BSN string `json:"bsn"`
}

func toPtr[T any](t T) *T {
	return &t
}

func (app *Application) FinancialClaim(w http.ResponseWriter, r *http.Request) {
	body := new(FinancialClaimBody)
	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		writeError(w, fmt.Errorf("decode failed: %w", err))
		return
	}

	var err error

	// Find the person in one of the municipalities
	person := new(model.Person)
	for key := range app.municipalities {
		person, err = app.municipalities[key].FRP.GetPersonBSN(r.Context(), &model.VwlLog{}, body.BSN)
		if err == nil {
			continue
		}
	}

	// If we cannot find the person return a default Financial claim
	// Should be removed in the future
	if person.ID == uuid.Nil {
		fmt.Println("Cannot find person, returning default")

		if err := json.NewEncoder(w).Encode(GetDefault(body.BSN)); err != nil {
			writeError(w, err)
			return
		}

		w.WriteHeader(http.StatusOK)
		return
	}

	records, err := app.db.Queries.AssessmentsListOwnerAll(r.Context(), person.ID)
	if err != nil {
		writeError(w, err)
		return
	}

	assessments := adapter.ToAssessments(records)

	// Seed the random number generator with the current time
	randr := rand.New(rand.NewSource(assessments[0].CreatedAt.UnixNano()))

	// Generate a random 17-digit number
	min := 10000000000000000 // 10^16
	max := 99999999999999999 // 10^17 - 1

	kenmerk := strconv.Itoa(randr.Intn(max-min+1) + min)

	verwerking := model.VwlLog{
		VerwerkingsActiviteit: "http://example.com/activity/v0/woz-ozb-b",
		VerwerkingsSpan:       uuid.New(),
	}

	document := model.FinancialClaimsInformationDocument{
		Type:    "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT",
		Version: "3.0.0",
		Body: model.Body{
			AangeleverdDoor:   OIN,
			DocumentDatumtijd: model.FinancialClaimTime(time.Now()),
			BSN:               body.BSN,
			FinancieleZaken:   []model.FinancieleZaak{},
		},
	}

	objects := make([]couple, 0, len(assessments))

	for idx := range assessments {
		record, err := app.db.Queries.TariffGet(r.Context(), assessments[idx].TariffID)
		if err != nil {
			writeError(w, err)
			return
		}

		tariff := adapter.ToTariff(record)

		api := app.municipalities[tariff.Municipality]

		wozObject := new(model.WOZObject)

		url := fmt.Sprintf("/woz-objects/%s", assessments[idx].WOZObjectID)
		if err := api.WOZ.Request(r.Context(), &verwerking, http.MethodGet, url, nil, wozObject); err != nil {
			writeError(w, fmt.Errorf("woz request: %w", err))
			return
		}

		var ownerID *uuid.UUID
		if wozObject.StakeholderOwner != nil {
			ownerID, _, _, err = app.GetStakeholder(r.Context(), &verwerking, &api, *wozObject.StakeholderOwner)
			if err != nil {
				writeError(w, fmt.Errorf("get stakeholder owner failed; %w", err))
				return
			}
		}

		var occupantID *uuid.UUID
		if wozObject.StakeholderOccupant != nil {
			occupantID, _, _, err = app.GetStakeholder(r.Context(), &verwerking, &api, *wozObject.StakeholderOccupant)
			if err != nil {
				writeError(w, fmt.Errorf("get stakeholder occupant failed: %w", err))
				return
			}
		}

		objects = append(objects, couple{
			assessments: &assessments[idx],
			wozObject:   wozObject,
			ownerID:     ownerID,
			occupantID:  occupantID,
			tariff:      tariff,
		})
	}

	for idx := range objects {
		gebeurtenissen := []model.Gebeurtenis{}

		tariff := objects[idx].tariff
		wozObject := objects[idx].wozObject
		assessment := objects[idx].assessments

		ozb := float64(wozObject.Values[0].Value) * tariff.PropertyTaxOwnershipNonResidential
		sewerage := tariff.SewerageTax

		wasteTax := tariff.WasteTaxOnePersonHousehold
		if wozObject.RegisteredPeople > 1 {
			wasteTax = tariff.WasteTaxMultiPersonHousehold
		}

		const (
			euroToCents = 100
		)

		total := int((ozb + sewerage + wasteTax) * euroToCents)

		gebeurtenissen = append(gebeurtenissen,
			model.Gebeurtenis{
				GebeurtenisType:                 "FinancieleVerplichtingOpgelegd",
				GebeurtenisKenmerk:              fmt.Sprintf("%s-1", kenmerk),
				DatumtijdGebeurtenis:            toPtr(model.FinancialClaimTime(time.Now())),
				DatumtijdOpgelegd:               toPtr(model.FinancialClaimTime(time.Now())),
				Zaakkenmerk:                     kenmerk,
				Beschikkingsnummer:              "99999999",
				BSN:                             body.BSN,
				PrimaireVerplichting:            toPtr(true),
				Type:                            "GEMEENTE_OZB_EIGENAAR",
				Categorie:                       "Algemeen",
				Bedrag:                          int(ozb * euroToCents),
				Omschrijving:                    fmt.Sprintf("Op basis van WOZ beschikking: %s", assessment.WOZAddressLine1),
				JuridischeGrondslagOmschrijving: "Ontroerend zaken belasting",
				JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0007119/2023-01-01",
				OpgelegdDoor:                    OIN,
				UitgevoerdDoor:                  OIN,
			},
			model.Gebeurtenis{
				GebeurtenisType:                 "FinancieleVerplichtingOpgelegd",
				GebeurtenisKenmerk:              fmt.Sprintf("%s-2", kenmerk),
				DatumtijdGebeurtenis:            toPtr(model.FinancialClaimTime(time.Now())),
				DatumtijdOpgelegd:               toPtr(model.FinancialClaimTime(time.Now())),
				Zaakkenmerk:                     kenmerk,
				Beschikkingsnummer:              "99999999",
				BSN:                             body.BSN,
				PrimaireVerplichting:            toPtr(true),
				Type:                            "GEMEENTE_RIOOLHEFFING",
				Categorie:                       "Algemeen",
				Bedrag:                          int(sewerage * euroToCents),
				Omschrijving:                    fmt.Sprintf("Op basis van rioolheffing: %s", assessment.WOZAddressLine1),
				JuridischeGrondslagOmschrijving: "Gemeentelijke rioolheffing",
				JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0007119/2023-01-01",
				OpgelegdDoor:                    OIN,
				UitgevoerdDoor:                  OIN,
			},
			model.Gebeurtenis{
				GebeurtenisType:                 "FinancieleVerplichtingOpgelegd",
				GebeurtenisKenmerk:              fmt.Sprintf("%s-3", kenmerk),
				DatumtijdGebeurtenis:            toPtr(model.FinancialClaimTime(time.Now())),
				DatumtijdOpgelegd:               toPtr(model.FinancialClaimTime(time.Now())),
				Zaakkenmerk:                     kenmerk,
				Beschikkingsnummer:              "99999999",
				BSN:                             body.BSN,
				PrimaireVerplichting:            toPtr(true),
				Type:                            "GEMEENTE_AFVALSTOFFENHEFFING",
				Categorie:                       "Algemeen",
				Bedrag:                          int(wasteTax * euroToCents),
				Omschrijving:                    fmt.Sprintf("Op basis van %s: %s", GetWasteTaxDescription(wozObject), assessment.WOZAddressLine1),
				JuridischeGrondslagOmschrijving: "Gemeentelijke afvalstoffenheffing",
				JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0007119/2023-01-01",
				OpgelegdDoor:                    OIN,
				UitgevoerdDoor:                  OIN,
			})

		zaak := model.FinancieleZaak{
			Zaakkenmerk:        assessments[idx].ID.String(),
			Omschrijving:       "Gemeentebelastingen",
			TotaalOpgelegd:     total,
			TotaalReedsBetaald: 0,
			Saldo:              total,
			SaldoDatumtijd:     model.FinancialClaimTime(time.Now()),
			Gebeurtenissen:     gebeurtenissen,
		}

		document.Body.FinancieleZaken = append(document.Body.FinancieleZaken, zaak)
	}

	if err := json.NewEncoder(w).Encode(document); err != nil {
		writeError(w, fmt.Errorf("encode failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

func GetWasteTaxDescription(object *model.WOZObject) string {
	if object.RegisteredPeople == 1 {
		return "eenpersoonshuishouden"
	}

	return "meerpersoonshuishouden"
}

func GetDefault(bsn string) *model.FinancialClaimsInformationDocument {
	// Generate a random 17-digit number
	min := 10000000000000000 // 10^16
	max := 99999999999999999 // 10^17 - 1

	randr := rand.New(rand.NewSource(time.Now().UnixNano()))

	kenmerk := strconv.Itoa(randr.Intn(max-min+1) + min)

	ozb := 42110
	sewerage := 23700
	wasteTax := 36625
	total := ozb + sewerage + wasteTax

	return &model.FinancialClaimsInformationDocument{
		Type:    "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT",
		Version: "3.0.0",
		Body: model.Body{
			AangeleverdDoor:   OIN,
			DocumentDatumtijd: model.FinancialClaimTime(time.Now()),
			BSN:               bsn,
			FinancieleZaken: []model.FinancieleZaak{
				{
					Zaakkenmerk:        kenmerk,
					Omschrijving:       "Gemeentebelastingen",
					TotaalOpgelegd:     total,
					TotaalReedsBetaald: 0,
					Saldo:              total,
					SaldoDatumtijd:     model.FinancialClaimTime(time.Now()),
					Gebeurtenissen: []model.Gebeurtenis{
						{
							GebeurtenisType:                 "FinancieleVerplichtingOpgelegd",
							GebeurtenisKenmerk:              fmt.Sprintf("%s-1", kenmerk),
							DatumtijdGebeurtenis:            toPtr(model.FinancialClaimTime(time.Now())),
							DatumtijdOpgelegd:               toPtr(model.FinancialClaimTime(time.Now())),
							Zaakkenmerk:                     kenmerk,
							Beschikkingsnummer:              "99999999",
							BSN:                             bsn,
							PrimaireVerplichting:            toPtr(true),
							Type:                            "GEMEENTE_OZB_EIGENAAR",
							Categorie:                       "Algemeen",
							Bedrag:                          ozb,
							Omschrijving:                    "Op basis van WOZ beschikking: Aastraat 1 Woudendijk",
							JuridischeGrondslagOmschrijving: "Ontroerend zaken belasting Artikel ",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0007119/2023-01-01",
							OpgelegdDoor:                    OIN,
							UitgevoerdDoor:                  OIN,
						},
						{
							GebeurtenisType:                 "FinancieleVerplichtingOpgelegd",
							GebeurtenisKenmerk:              fmt.Sprintf("%s-2", kenmerk),
							DatumtijdGebeurtenis:            toPtr(model.FinancialClaimTime(time.Now())),
							DatumtijdOpgelegd:               toPtr(model.FinancialClaimTime(time.Now())),
							Zaakkenmerk:                     kenmerk,
							Beschikkingsnummer:              "99999999",
							BSN:                             bsn,
							PrimaireVerplichting:            toPtr(true),
							Type:                            "GEMEENTE_RIOOLHEFFING",
							Categorie:                       "Algemeen",
							Bedrag:                          sewerage,
							Omschrijving:                    "Op basis van rioolheffing: Aastraat 1 woudendijk",
							JuridischeGrondslagOmschrijving: "Ontroerend zaken belasting Artikel ",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0007119/2023-01-01",
							OpgelegdDoor:                    OIN,
							UitgevoerdDoor:                  OIN,
						},
						{
							GebeurtenisType:                 "FinancieleVerplichtingOpgelegd",
							GebeurtenisKenmerk:              fmt.Sprintf("%s-3", kenmerk),
							DatumtijdGebeurtenis:            toPtr(model.FinancialClaimTime(time.Now())),
							DatumtijdOpgelegd:               toPtr(model.FinancialClaimTime(time.Now())),
							Zaakkenmerk:                     kenmerk,
							Beschikkingsnummer:              "99999999",
							BSN:                             bsn,
							PrimaireVerplichting:            toPtr(true),
							Type:                            "GEMEENTE_AFVALSTOFFENHEFFING",
							Categorie:                       "Algemeen",
							Bedrag:                          wasteTax,
							Omschrijving:                    "Op basis van meerpersoonsafvalstoffenheffing: Aastraat 1 woudendijk",
							JuridischeGrondslagOmschrijving: "Ontroerend zaken belasting Artikel ",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0007119/2023-01-01",
							OpgelegdDoor:                    OIN,
							UitgevoerdDoor:                  OIN,
						},
						{
							GebeurtenisType:              "BetalingsverplichtingOpgelegd",
							GebeurtenisKenmerk:           fmt.Sprintf("%s-4", kenmerk),
							DatumtijdGebeurtenis:         toPtr(model.FinancialClaimTime(time.Now())),
							DatumtijdOpgelegd:            toPtr(model.FinancialClaimTime(time.Now())),
							Zaakkenmerk:                  kenmerk,
							BSN:                          bsn,
							Bedrag:                       total,
							Betaalwijze:                  "Handmatig",
							TeBetalenAan:                 OIN,
							Rekeningnummer:               "GB33BUKB20201555555555",
							RekeningnummerTenaamstelling: "Belastingsamenwerking Zonnenwoud",
							Betalingskenmerk:             kenmerk,
							Vervaldatum:                  toPtr(model.FinancialClaimTime(time.Now().AddDate(0, 1, 0))),
						},
					},
					Achterstanden: []string{},
				},
			},
		},
	}
}
