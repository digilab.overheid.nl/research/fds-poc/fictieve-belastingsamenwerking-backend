//nolint:tagliatelle // not our standard
package model

import (
	"encoding/json"
	"time"
)

type FinancialClaimTime time.Time

func (t FinancialClaimTime) MarshalJSON() ([]byte, error) {
	t2 := time.Time(t).Format("2006-01-02T15:04:05+00:00")

	return json.Marshal(t2)
}

type Gebeurtenis struct {
	GebeurtenisType                 string              `json:"gebeurtenis_type"`
	GebeurtenisKenmerk              string              `json:"gebeurtenis_kenmerk"`
	DatumtijdVerwerkt               *FinancialClaimTime `json:"datumtijd_verwerkt,omitempty"`
	DatumtijdGebeurtenis            *FinancialClaimTime `json:"datumtijd_gebeurtenis,omitempty"`
	DatumtijdOpgelegd               *FinancialClaimTime `json:"datumtijd_opgelegd,omitempty"`
	DatumtijdOntvangen              *FinancialClaimTime `json:"datumtijd_ontvangen,omitempty"`
	Zaakkenmerk                     string              `json:"zaakkenmerk,omitempty"`
	Beschikkingsnummer              string              `json:"beschikkingsnummer,omitempty"`
	BSN                             string              `json:"bsn"`
	PrimaireVerplichting            *bool               `json:"primaire_verplichting,omitempty"`
	Type                            string              `json:"type,omitempty"`
	Categorie                       string              `json:"categorie,omitempty"`
	Bedrag                          int                 `json:"bedrag,omitempty"`
	Omschrijving                    string              `json:"omschrijving,omitempty"`
	JuridischeGrondslagOmschrijving string              `json:"juridische_grondslag_omschrijving,omitempty"`
	JuridischeGrondslagBron         string              `json:"juridische_grondslag_bron,omitempty"`
	OpgelegdDoor                    string              `json:"opgelegd_door,omitempty"`
	UitgevoerdDoor                  string              `json:"uitgevoerd_door,omitempty"`
	Betaalwijze                     string              `json:"betaalwijze,omitempty"`
	TeBetalenAan                    string              `json:"te_betalen_aan,omitempty"`
	Rekeningnummer                  string              `json:"rekeningnummer,omitempty"`
	RekeningnummerTenaamstelling    string              `json:"rekeningnummer_tenaamstelling,omitempty"`
	Betalingskenmerk                string              `json:"betalingskenmerk,omitempty"`
	Vervaldatum                     *FinancialClaimTime `json:"vervaldatum,omitempty"`
}

type FinancieleZaak struct {
	Zaakkenmerk        string             `json:"zaakkenmerk"`
	Omschrijving       string             `json:"omschrijving"`
	TotaalOpgelegd     int                `json:"totaal_opgelegd"`
	TotaalReedsBetaald int                `json:"totaal_reeds_betaald"`
	Saldo              int                `json:"saldo"`
	SaldoDatumtijd     FinancialClaimTime `json:"saldo_datumtijd"`
	Gebeurtenissen     []Gebeurtenis      `json:"gebeurtenissen"`
	Achterstanden      []string           `json:"achterstanden"`
}

type Body struct {
	AangeleverdDoor   string             `json:"aangeleverd_door"`
	DocumentDatumtijd FinancialClaimTime `json:"document_datumtijd"`
	BSN               string             `json:"bsn"`
	FinancieleZaken   []FinancieleZaak   `json:"financiele_zaken"`
}

type FinancialClaimsInformationDocument struct {
	Type    string `json:"type"`
	Version string `json:"version"`
	Body    Body   `json:"body"`
}
