package model

import (
	"time"

	"github.com/google/uuid"
)

type OZB struct {
	ID           uuid.UUID
	AddressID    uuid.UUID
	BuildingID   uuid.UUID
	PersonID     uuid.UUID
	AddressLabel string
	PersonLabel  string
	WozAmount    int32
	CreatedAt    time.Time
}
