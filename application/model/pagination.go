package model

import "github.com/google/uuid"

type Pagination struct {
	FirstID *uuid.UUID `json:"firstId"`
	LastID  *uuid.UUID `json:"lastId"`
	PerPage int        `json:"perPage"`
}

type Response[T any] struct {
	Data       []T         `json:"data"`
	Pagination *Pagination `json:"pagination"`
}
