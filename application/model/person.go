package model

import (
	"time"

	"github.com/google/uuid"
)

type Person struct {
	ID                  uuid.UUID  `json:"id"`
	BSN                 string     `json:"bsn,omitempty"`
	Name                string     `json:"name,omitempty"`
	BornAt              *time.Time `json:"bornAt,omitempty"`
	Sex                 string     `json:"sex,omitempty"`
	DiedAt              *time.Time `json:"diedAt,omitempty"`
	RegisteredAddressID *uuid.UUID `json:"registeredAddressId,omitempty"`
	Partners            []Person   `json:"partners,omitempty"`
	Children            []Person   `json:"children,omitempty"`
	Parents             []Person   `json:"parents,omitempty"`
}
