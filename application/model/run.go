package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Status int

const (
	RunStatusInactive        Status = 0
	RunStatusBatchRunRunning Status = 1
	RunStatusBatchRunDone    Status = 2
	RunStatusFinalizeRunning Status = 3
	RunStatusFinalizeDone    Status = 4
)

func (item Status) MarshalJSON() ([]byte, error) {
	value := ""

	switch item {
	case RunStatusInactive:
		value = "inactive"
	case RunStatusBatchRunRunning:
		value = "batch_run_running"
	case RunStatusBatchRunDone:
		value = "batch_run_done"
	case RunStatusFinalizeRunning:
		value = "finalize_running"
	case RunStatusFinalizeDone:
		value = "finalize_done"
	}

	return json.Marshal(value)
}

type Run struct {
	ID        uuid.UUID `json:"id"`
	Year      int       `json:"year"`
	Status    Status    `json:"status"`
	CreatedAt time.Time `json:"createdAt"`
}

func (item Run) GetID() uuid.UUID {
	return item.ID
}
