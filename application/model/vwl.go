package model

import "github.com/google/uuid"

type VwlConfig struct {
	VwlService        string
	Systeem           string
	Verantwoordelijke string
	Verwerker         string
}

type VwlLog struct {
	VerwerkingsActiviteit string
	VerwerkingsSpan       uuid.UUID
}

type VwlLogMessage struct {
	VerwerkingsActiviteit string    `json:"verwerkings_activiteit"`
	VerwerkingsSpan       uuid.UUID `json:"verwerkings_span"`
	Systeem               string    `json:"systeem"`
	Verantwoordelijke     string    `json:"verantwoordelijke"`
	Verwerker             string    `json:"verwerker"`
}

type VwlLogMessageResponse struct {
	Created               string    `json:"created"`
	VerwerkingsActiviteit string    `json:"verwerkings_activiteit"`
	VerwerkingsSpan       uuid.UUID `json:"verwerkings_span"`
	Systeem               string    `json:"systeem"`
	Verantwoordelijke     string    `json:"verantwoordelijke"`
	Verwerker             string    `json:"verwerker"`
}
