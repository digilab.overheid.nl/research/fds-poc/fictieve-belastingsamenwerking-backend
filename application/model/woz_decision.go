package model

import (
	"time"

	"github.com/google/uuid"
)

type WozDecision struct {
	ID          uuid.UUID `json:"id"`
	RunID       uuid.UUID `json:"runId"`
	WozObjectID uuid.UUID `json:"wozObjectId"`
	Amount      int       `json:"amount"`
	CreatedAt   time.Time `json:"createdAt"`
}
