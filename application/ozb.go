package application

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/meta"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/pagination"
)

func (app *Application) RunList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*queries.DigilabDemoFbsRun

	switch md.IsASC() {
	case true:
		records, err = app.runListAsc(r.Context(), md)
	case false:
		records, err = app.runListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	objects := adapter.ToRuns(records)

	response, err := pagination.Build(objects, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) runListAsc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFbsRun, error) {
	params := &queries.RunListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.RunListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("tax bill list failed: %w", err)
	}

	return records, nil
}

func (app *Application) runListDesc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFbsRun, error) {
	params := &queries.RunListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.RunListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("tax bill list failed: %w", err)
	}

	return records, nil
}

func (app *Application) RunGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	record, err := app.db.Queries.RunGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	item := adapter.ToRun(record)

	if err := json.NewEncoder(w).Encode(item); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) RunGetTariffs(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	records, err := app.db.Queries.TariffListRun(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	tariffs := make([]model.Tariff, 0, len(records))
	for idx := range records {
		tariffs = append(tariffs, model.Tariff{
			ID:                                 records[idx].ID,
			RunID:                              records[idx].RunID,
			Municipality:                       records[idx].Municipality,
			SewerageTax:                        records[idx].SewerageTax,
			PropertyTaxOwnershipResidential:    records[idx].PropertyTaxOwnerResidential,
			PropertyTaxOwnershipNonResidential: records[idx].PropertyTaxOwnerNonResidential,
			PropertyTaxUsageNonResidential:     records[idx].PropertyTaxUsageNonResidential,
			WasteTaxOnePersonHousehold:         records[idx].WasteTaxOnePersonHousehold,
			WasteTaxMultiPersonHousehold:       records[idx].WasteTaxMultiPersonHousehold,
		})
	}

	if err := json.NewEncoder(w).Encode(tariffs); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) RunCreate(w http.ResponseWriter, r *http.Request) {
	tariffs := new(model.Tariffs)
	if err := json.NewDecoder(r.Body).Decode(tariffs); err != nil {
		writeError(w, fmt.Errorf("decode failed: %w", err))
		return
	}

	runID := uuid.New()

	params := &queries.RunCreateParams{
		ID:     runID,
		Year:   int32(tariffs.Year),
		Status: int32(model.RunStatusBatchRunRunning),
	}

	if err := app.db.Queries.RunCreate(r.Context(), params); err != nil {
		writeError(w, fmt.Errorf("run create: %w", err))
		return
	}

	app.ozbRunStatus.Write(runID, Work{})

	for municipality := range tariffs.Tariffs {
		municipality := municipality

		params := &queries.TariffCreateParams{
			ID:                             uuid.New(),
			RunID:                          params.ID,
			Municipality:                   municipality,
			PropertyTaxOwnerResidential:    tariffs.Tariffs[municipality].PropertyTaxOwnershipResidential,
			PropertyTaxOwnerNonResidential: tariffs.Tariffs[municipality].PropertyTaxOwnershipNonResidential,
			PropertyTaxUsageNonResidential: tariffs.Tariffs[municipality].PropertyTaxUsageNonResidential,
			SewerageTax:                    tariffs.Tariffs[municipality].SewerageTax,
			WasteTaxOnePersonHousehold:     tariffs.Tariffs[municipality].WasteTaxOnePersonHousehold,
			WasteTaxMultiPersonHousehold:   tariffs.Tariffs[municipality].WasteTaxMultiPersonHousehold,
		}

		if err := app.db.Queries.TariffCreate(r.Context(), params); err != nil {
			writeError(w, fmt.Errorf("tariff create: %w", err))
			return
		}

		go func() {
			tariff := tariffs.Tariffs[municipality]

			if err := app.doRun2(context.Background(), runID, params.ID, municipality, &tariff); err != nil {
				fmt.Print(fmt.Errorf("do run failed: %w", err))
			}
		}()
	}

	var ider struct {
		ID uuid.UUID `json:"id"`
	}

	ider.ID = params.ID
	if err := json.NewEncoder(w).Encode(ider); err != nil {
		writeError(w, fmt.Errorf("encode failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (app *Application) RunStatus(w http.ResponseWriter, r *http.Request) {
	runID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, fmt.Errorf("uuid parse failed: %w", err))
		return
	}

	work, ok := app.ozbRunStatus.Read(runID)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err := json.NewEncoder(w).Encode(work); err != nil {
		writeError(w, fmt.Errorf("encode failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}
