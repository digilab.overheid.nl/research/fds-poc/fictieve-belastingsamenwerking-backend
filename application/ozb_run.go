package application

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
)

func (app *Application) doRun(ctx context.Context, runID, tariffID uuid.UUID, municipality string, tarrifs *model.Tariff) error {
	api := app.municipalities[municipality]

	workers := 32
	numJobs := 1000

	work := make(chan *model.WOZObject, numJobs)
	errs := make(chan error, numJobs)

	var wg sync.WaitGroup

	for i := 0; i < workers; i++ {
		i := i

		wg.Add(1)

		go func() {
			app.worker(ctx, &api, runID, tariffID, i, tarrifs, work, errs)
			wg.Done()
		}()
	}

	go func() {
		for err := range errs {
			if err := app.ozbRunStatus.IncreaseError(runID, 1); err != nil {
				fmt.Printf("increase error: %v", err)
			}

			fmt.Printf("err: %+v", err)
		}
	}()

	wozObjects := model.Response[*model.WOZObject]{}

	uri := url.Values{}
	uri.Add("perPage", "1000")

	if err := api.WOZ.Request(ctx, nil, http.MethodGet, fmt.Sprintf("/woz-objects?%s", uri.Encode()), nil, &wozObjects); err != nil {
		return fmt.Errorf("woz request failed: %w", err)
	}

	for i := 0; i < 50000; i++ {
		entry := wozObjects.Data[0]
		entry.ID = uuid.New()
		wozObjects.Data = append(wozObjects.Data, entry)
	}

	if err := app.ozbRunStatus.IncreaseAmount(runID, len(wozObjects.Data)); err != nil {
		return fmt.Errorf("increase amount: %w", err)
	}

	for idx := range wozObjects.Data {
		work <- wozObjects.Data[idx]
	}

	close(work)
	wg.Wait()
	close(errs)

	if err := app.ozbRunStatus.SetFinished(runID, true); err != nil {
		return fmt.Errorf("set finished: %w", err)
	}

	return nil
}

func (app *Application) worker(ctx context.Context, api *Municipality, runID, tariffID uuid.UUID, workerID int, tariff *model.Tariff, jobs <-chan *model.WOZObject, results chan<- error) {
	fmt.Printf("Running worker %d\n", workerID)

	for job := range jobs {
		if err := app.work(ctx, api, runID, tariffID, job); err != nil {
			results <- err
		}
	}

	fmt.Printf("Worker %d done\n", workerID)
}

var (
	ErrWOZObjectNoValue  = errors.New("woz-object should contain value")
	ErrOwnerHasNoAddress = errors.New("owner is not registered on address")
)

func (app *Application) work(
	ctx context.Context,
	api *Municipality,
	runID, tariffID uuid.UUID,
	object *model.WOZObject,
) error {
	if len(object.Values) < 1 {
		return ErrWOZObjectNoValue
	}

	verwerking := model.VwlLog{
		VerwerkingsActiviteit: "http://example.com/activity/v0/woz-subject-data",
		VerwerkingsSpan:       uuid.New(),
	}

	decisionID := uuid.New()
	if err := app.db.Queries.DecisionCreate(ctx, adapter.FromDecisionCreate(decisionID, runID, object.ID, int32(object.Values[0].Value))); err != nil {
		return fmt.Errorf("decision create: %w", err)
	}

	// Grab address from object
	address := new(model.Address)
	if err := app.frag.Request(ctx, &verwerking, http.MethodGet, fmt.Sprintf("/addresses/%s", object.AddressableObjectID), nil, address); err != nil {
		return fmt.Errorf("frag request: %w", err)
	}

	stakeholders := make([]model.Stakeholder, 0)
	if object.StakeholderOwner != nil {
		stakeholders = append(stakeholders, *object.StakeholderOwner)
	}

	if object.StakeholderOccupant != nil {
		stakeholders = append(stakeholders, *object.StakeholderOccupant)
	}

	for _, v := range stakeholders {
		stakeholderID, stakeholderLabel, stakeholderAddressID, err := app.GetStakeholder(ctx, &verwerking, api, v)
		if err != nil {
			return err
		}

		ownerAddress, err := app.GetAddress(ctx, &verwerking, *stakeholderAddressID)
		if err != nil {
			return err
		}

		params := &queries.AssessmentCreateParams{
			ID:             uuid.New(),
			TariffID:       tariffID,
			DecisionID:     decisionID,
			WozObjectID:    object.ID,
			OwnerType:      string(object.StakeholderOwner.Type),
			OwnerID:        *stakeholderID,
			OwnerLabel:     stakeholderLabel,
			OwnerAddressID: ownerAddress.ID,
			OwnerAddressLine1: fmt.Sprintf("%s %d%s %s",
				ownerAddress.Street, ownerAddress.HouseNumber, ownerAddress.HouseNumberAddition, ownerAddress.Municipality.Name),
			OwnerAddressLine2: ownerAddress.ZipCode,
			WozAddressID:      address.ID,
			WozAddressLine1: fmt.Sprintf("%s %d%s %s",
				address.Street, address.HouseNumber, address.HouseNumberAddition, address.Municipality.Name),
			WozAddressLine2:  address.ZipCode,
			WozAmount:        int32(object.Values[0].Value),
			RegisteredPeople: int32(object.RegisteredPeople),
			CreatedAt:        time.Now(),
			EffectiveAt:      time.Now(),
		}

		if err := app.db.Queries.AssessmentCreate(ctx, params); err != nil {
			return fmt.Errorf("assessment create: %w", err)
		}
	}

	if err := app.ozbRunStatus.IncreaseDone(runID, 1); err != nil {
		return fmt.Errorf("increased done: %w", err)
	}

	return nil
}

func (app *Application) GetStakeholder(
	ctx context.Context,
	verwerking *model.VwlLog,
	api *Municipality,
	stakeholder model.Stakeholder,
) (stakeholderID *uuid.UUID, stakeholderLabel string, address *uuid.UUID, err error) {
	var addressID *uuid.UUID

	switch stakeholder.Type {
	case model.NaturalPerson:
		person, err := api.FRP.GetPersonBSN(ctx, verwerking, stakeholder.BSN)
		if err != nil {
			return nil, "", nil, fmt.Errorf("brp request: %w", err)
		}

		stakeholderID = &person.ID
		stakeholderLabel = person.Name
		addressID = person.RegisteredAddressID
	case model.NonNaturalPerson:
	}

	if addressID == nil {
		return nil, "", nil, ErrOwnerHasNoAddress
	}

	return stakeholderID, stakeholderLabel, addressID, nil
}

func (app *Application) GetAddress(ctx context.Context, verwerking *model.VwlLog, id uuid.UUID) (*model.Address, error) {
	address := new(model.Address)
	if err := app.frag.Request(ctx, verwerking, http.MethodGet, fmt.Sprintf("/addresses/%s", id), nil, address); err != nil {
		return nil, fmt.Errorf("frag request: %w", err)
	}

	return address, nil
}
