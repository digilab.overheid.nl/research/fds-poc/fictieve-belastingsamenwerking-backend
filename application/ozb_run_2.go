package application

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"runtime"
	"sync"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
	"golang.org/x/sync/semaphore"
)

func (app *Application) doRun2(ctx context.Context, runID, tariffID uuid.UUID, municipality string, tarrifs *model.Tariff) error {
	var (
		maxWorkers = runtime.GOMAXPROCS(0)
		sem        = semaphore.NewWeighted(int64(maxWorkers))
	)

	work := make(chan *model.WOZObject, 1000)
	errs := make(chan error, 1000)

	api := app.municipalities[municipality]
	a := &api

	var wg sync.WaitGroup

	go func() {
		// Compute the output using up to maxWorkers goroutines at a time.
		for i := range work {
			// When maxWorkers goroutines are in flight, Acquire blocks until one of the
			// workers finishes.
			if err := sem.Acquire(ctx, 1); err != nil {
				log.Printf("Failed to acquire semaphore: %v", err)
				break
			}

			go func(object *model.WOZObject) {
				defer sem.Release(1)

				if err := app.work(ctx, a, runID, tariffID, object); err != nil {
					errs <- err
				}

				wg.Done()
			}(i)
		}

	}()

	go func() {
		for err := range errs {
			if err := app.ozbRunStatus.IncreaseError(runID, 1); err != nil {
				fmt.Printf("increase error: %v", err)
			}

			fmt.Printf("err: %+v", err)
		}
	}()

	verwerking := &model.VwlLog{
		VerwerkingsActiviteit: "http://example.com/activity/v0/woz-ozb-batch",
		VerwerkingsSpan:       uuid.New(),
	}

	var lastID *uuid.UUID
	firstTime := true

	for lastID != nil || firstTime {
		wozObjects := model.Response[*model.WOZObject]{}
		firstTime = false

		uri := url.Values{}
		uri.Add("perPage", "1000")
		if lastID != nil {
			uri.Add("lastId", lastID.String())
		}

		if err := api.WOZ.Request(ctx, verwerking, http.MethodGet, fmt.Sprintf("/woz-objects?%s", uri.Encode()), nil, &wozObjects); err != nil {
			return fmt.Errorf("woz request failed: %w", err)
		}

		if wozObjects.Pagination != nil {
			lastID = wozObjects.Pagination.LastID
		}

		if err := app.ozbRunStatus.IncreaseAmount(runID, len(wozObjects.Data)); err != nil {
			return fmt.Errorf("increase amount: %w", err)
		}

		wg.Add(len(wozObjects.Data))

		for idx := range wozObjects.Data {
			work <- wozObjects.Data[idx]
		}
	}

	close(work)
	wg.Wait()
	close(errs)

	if err := app.ozbRunStatus.SetFinished(runID, true); err != nil {
		return fmt.Errorf("set finished: %w", err)
	}

	if err := app.db.Queries.RunUpdate(ctx, &generated.RunUpdateParams{Status: int32(model.RunStatusBatchRunDone), ID: runID}); err != nil {
		return fmt.Errorf("run update failed: %w", err)
	}

	return nil
}
