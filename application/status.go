package application

import (
	"sync"
	"time"

	"github.com/google/uuid"
)

type Work struct {
	Amount     int           `json:"amount"`
	Done       int           `json:"done"`
	Error      int           `json:"error"`
	Finished   bool          `json:"finished"`
	CreatedAt  time.Time     `json:"-"`
	FinishedIn time.Duration `json:"finishedIn"`
}

type Status struct {
	status map[uuid.UUID]Work
	lock   sync.RWMutex
}

func (status *Status) Read(id uuid.UUID) (Work, bool) {
	status.lock.RLock()
	defer status.lock.RUnlock()

	if entry, ok := status.status[id]; ok {
		return entry, ok
	}

	return Work{}, false
}

func (status *Status) Write(id uuid.UUID, work Work) {
	status.lock.Lock()
	defer status.lock.Unlock()

	work.CreatedAt = time.Now()

	status.status[id] = work
}

func (status *Status) IncreaseAmount(id uuid.UUID, amount int) error {
	status.lock.Lock()
	defer status.lock.Unlock()

	if entry, ok := status.status[id]; ok {
		entry.Amount += amount
		status.status[id] = entry
	}

	return nil
}

func (status *Status) IncreaseDone(id uuid.UUID, done int) error {
	status.lock.Lock()
	defer status.lock.Unlock()

	if entry, ok := status.status[id]; ok {
		entry.Done += done
		status.status[id] = entry
	}

	return nil
}

func (status *Status) IncreaseError(id uuid.UUID, amount int) error {
	status.lock.Lock()
	defer status.lock.Unlock()

	if entry, ok := status.status[id]; ok {
		entry.Error += amount
		status.status[id] = entry
	}

	return nil
}

func (status *Status) SetFinished(id uuid.UUID, finished bool) error {
	status.lock.Lock()
	defer status.lock.Unlock()

	if entry, ok := status.status[id]; ok {
		entry.Finished = finished
		entry.FinishedIn = time.Since(entry.CreatedAt)
		status.status[id] = entry
	}

	return nil
}
