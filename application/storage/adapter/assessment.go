package adapter

import (
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
)

func ToAssessment(record *queries.DigilabDemoFbsAssessment) *model.Assessment {
	return &model.Assessment{
		ID:                record.ID,
		TariffID:          record.TariffID,
		DecisionID:        record.DecisionID,
		WOZObjectID:       record.WozObjectID,
		OwnerType:         model.StakeholderType(record.OwnerType),
		OwnerID:           record.OwnerID,
		OwnerLabel:        record.OwnerLabel,
		OwnerAddressID:    record.OwnerAddressID,
		OwnerAddressLine1: record.OwnerAddressLine1,
		OwnerAddressLine2: record.OwnerAddressLine2,
		WOZAddressID:      record.WozAddressID,
		WOZAddressLine1:   record.WozAddressLine1,
		WOZAddressLine2:   record.WozAddressLine2,
		WOZAmount:         record.WozAmount,
		RegisteredPeople:  record.RegisteredPeople,
		CreatedAt:         record.CreatedAt,
	}
}

func ToAssessments(records []*queries.DigilabDemoFbsAssessment) []model.Assessment {
	assessments := make([]model.Assessment, 0, len(records))

	for idx := range records {
		assessments = append(assessments, *ToAssessment(records[idx]))
	}

	return assessments
}
