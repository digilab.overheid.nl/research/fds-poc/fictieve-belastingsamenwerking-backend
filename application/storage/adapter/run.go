package adapter

import (
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
)

func ToRun(record *queries.DigilabDemoFbsRun) *model.Run {
	return &model.Run{
		ID:        record.ID,
		Year:      int(record.Year),
		CreatedAt: record.CreatedAt,
		Status:    model.Status(record.Status),
	}
}

func ToRuns(records []*queries.DigilabDemoFbsRun) []model.Run {
	runs := make([]model.Run, 0, len(records))

	for idx := range records {
		runs = append(runs, *ToRun(records[idx]))
	}

	return runs
}
