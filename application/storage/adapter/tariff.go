package adapter

import (
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
)

func ToTariff(record *generated.DigilabDemoFbsTariff) *model.Tariff {
	return &model.Tariff{
		ID:                                 record.ID,
		RunID:                              record.RunID,
		Municipality:                       record.Municipality,
		PropertyTaxOwnershipResidential:    record.PropertyTaxOwnerResidential,
		PropertyTaxOwnershipNonResidential: record.PropertyTaxOwnerNonResidential,
		PropertyTaxUsageNonResidential:     record.PropertyTaxUsageNonResidential,
		SewerageTax:                        record.SewerageTax,
		WasteTaxOnePersonHousehold:         record.WasteTaxOnePersonHousehold,
		WasteTaxMultiPersonHousehold:       record.WasteTaxMultiPersonHousehold,
	}
}

func ToTariffs(records []*generated.DigilabDemoFbsTariff) []model.Tariff {
	tariffs := make([]model.Tariff, 0, len(records))

	for idx := range records {
		tariffs = append(tariffs, *ToTariff(records[idx]))
	}

	return tariffs
}
