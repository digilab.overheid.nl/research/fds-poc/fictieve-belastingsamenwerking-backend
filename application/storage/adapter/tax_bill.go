package adapter

import (
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
)

func ToTaxBill(record *queries.DigilabDemoFbsTaxBill) *model.TaxBill {
	return &model.TaxBill{
		ID:         record.ID,
		RunID:      record.RunID,
		OwnerType:  record.OwnerType,
		OwnerID:    record.OwnerID,
		OwnerLabel: record.OwnerLabel,
		CreatedAt:  record.CreatedAt,
	}
}

func ToTaxBills(records []*queries.DigilabDemoFbsTaxBill) []model.TaxBill {
	taxBills := make([]model.TaxBill, 0, len(records))

	for idx := range records {
		taxBills = append(taxBills, *ToTaxBill(records[idx]))
	}

	return taxBills
}
