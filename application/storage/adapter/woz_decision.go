package adapter

import (
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
)

func FromDecisionCreate(id, runID, wozObjectID uuid.UUID, amount int32) *queries.DecisionCreateParams {
	return &queries.DecisionCreateParams{
		ID:          id,
		RunID:       runID,
		WozObjectID: wozObjectID,
		Amount:      amount,
	}
}

func ToDecision(record *queries.DigilabDemoFbsDecision) *model.WozDecision {
	return &model.WozDecision{
		ID:          record.ID,
		RunID:       record.RunID,
		WozObjectID: record.WozObjectID,
		Amount:      int(record.Amount),
		CreatedAt:   record.CreatedAt.Time,
	}
}
