BEGIN transaction;

CREATE SCHEMA IF NOT EXISTS digilab_demo_fbs;

CREATE TABLE digilab_demo_fbs.woz_orders (
    id UUID NOT NULL,
    woz_object_id UUID NOT NULL,
    woz_amount INT NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_fbs.runs (
    id UUID NOT NULL,
    internal_id SERIAL NOT NULL,
    year INT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    
    PRIMARY KEY(id)
); 

CREATE TABLE digilab_demo_fbs.tariffs (
    id UUID NOT NULL,
    run_id UUID NOT NULL,

    municipality TEXT NOT NULL,
    
    property_tax_owner_residential DOUBLE PRECISION NOT NULL,
    property_tax_owner_non_residential DOUBLE PRECISION NOT NULL,
    property_tax_usage_non_residential DOUBLE PRECISION NOT NULL, 

    sewerage_tax DOUBLE PRECISION NOT NULL,
    waste_tax_one_person_household DOUBLE PRECISION NOT NULL,
    waste_tax_multi_person_household DOUBLE PRECISION NOT NULL,

    PRIMARY KEY(id),

    CONSTRAINT fk_tariffs_run_id FOREIGN KEY(run_id) REFERENCES digilab_demo_fbs.runs(id)
); 

CREATE TABLE digilab_demo_fbs.assessments (
    id UUID NOT NULL,
    internal_id SERIAL NOT NULL,
    tariff_id UUID NOT NULL,
    woz_order_id UUID NOT NULL,

    woz_object_id UUID NOT NULL,
    owner_type TEXT NOT NULL,
    owner_id UUID NOT NULL,
    owner_label TEXT NOT NULL,
    owner_address_id UUID NOT NULL,
    owner_address_line_1 TEXT NOT NULL,
    owner_address_line_2 TEXT NOT NULL,

    woz_address_id UUID NOT NULL,
    woz_address_line_1 TEXT NOT NULL,
    woz_address_line_2 TEXT NOT NULL,

    woz_amount INT NOT NULL,

    registered_people INT NOT NULL,

    created_at TIMESTAMP NOT NULL,
    effective_at TIMESTAMP NOT NULL,

    PRIMARY KEY(id),

    CONSTRAINT fk_assessments_tariff_id FOREIGN KEY(tariff_id) REFERENCES digilab_demo_fbs.tariffs(id)
);

 
COMMIT;
