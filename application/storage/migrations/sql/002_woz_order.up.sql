BEGIN;

DROP TABLE digilab_demo_fbs.woz_orders;

CREATE TABLE digilab_demo_fbs.decisions (
    id UUID NOT NULL,
    run_id UUID NOT NULL,
    woz_object_id UUID NOT NULL,
    amount INT NOT NULL,
    created_at TIMESTAMP DEFAULT(NOW()),

    PRIMARY KEY(id),

    CONSTRAINT fk_decisions_run_id FOREIGN KEY(run_id) REFERENCES digilab_demo_fbs.runs(id)
);

ALTER TABLE digilab_demo_fbs.assessments
RENAME COLUMN woz_order_id TO decision_id;

COMMIT;