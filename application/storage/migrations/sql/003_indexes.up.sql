BEGIN;

CREATE INDEX runs_internal_id ON digilab_demo_fbs.runs(internal_id);
CREATE INDEX tariffs_run_id ON digilab_demo_fbs.tariffs(run_id);
CREATE INDEX assessments_internal_id ON digilab_demo_fbs.assessments(internal_id);
CREATE INDEX assessments_tariff_id ON digilab_demo_fbs.assessments(tariff_id);
CREATE INDEX assessments_woz_order_id ON digilab_demo_fbs.assessments(decision_id);
CREATE INDEX assessments_woz_object_id ON digilab_demo_fbs.assessments(woz_object_id);
CREATE INDEX assessments_owner_id ON digilab_demo_fbs.assessments(owner_id);
CREATE INDEX assessments_owner_address_id ON digilab_demo_fbs.assessments(owner_address_id);
CREATE INDEX assessments_woz_address_id ON digilab_demo_fbs.assessments(woz_address_id);
CREATE INDEX decisions_run_id ON digilab_demo_fbs.decisions(run_id);
CREATE INDEX decisions_woz_object_id ON digilab_demo_fbs.decisions(woz_object_id);

COMMIT;