BEGIN;

CREATE TABLE digilab_demo_fbs.tax_bills (
    id UUID NOT NULL,
    internal_id SERIAL NOT NULL,
    run_id UUID NOT NULL,
    owner_type TEXT NOT NULL,
    owner_id UUID NOT NULL,
    owner_label TEXT NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT(NOW()),

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_fbs.tax_bill_assessment (
    tax_bill_id UUID NOT NULL,
    assessment_id UUID NOT NULL,

    PRIMARY KEY(tax_bill_id, assessment_id)
);

CREATE INDEX tax_bills_internal_id ON tax_bills(internal_id);
CREATE INDEX tax_bill_assessment_tax_bill_id ON tax_bill_assessment(tax_bill_id);
CREATE INDEX tax_bill_assessment_assessment_id ON tax_bill_assessment(assessment_id);

COMMIT;