-- name: DecisionCreate :exec
INSERT INTO digilab_demo_fbs.decisions
(id, run_id, woz_object_id, amount) 
VALUES (
    $1, $2, $3, $4
);

-- name: DecisionsBasedOnWozObject :many
SELECT id, run_id, woz_object_id, amount, created_at
FROM digilab_demo_fbs.decisions
WHERE woz_object_id=$1;