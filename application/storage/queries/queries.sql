-- name: AssessmentListAsc :many
SELECT objs.* FROM (
    SELECT id, internal_id, tariff_id, decision_id, woz_object_id, 
        owner_type, owner_id, owner_label,
        owner_address_id, owner_address_line_1, owner_address_line_2,
        woz_address_id, woz_address_line_1, woz_address_line_2,
        woz_amount, registered_people, created_at, effective_at
    FROM digilab_demo_fbs.assessments    
    WHERE assessments.internal_id > (
        SELECT sub_qry.internal_id
        FROM digilab_demo_fbs.assessments as sub_qry
        WHERE sub_qry.id=$1
    )
    ORDER BY assessments.internal_id ASC
    LIMIT $2
) objs ORDER BY objs.internal_id DESC;

-- name: AssessmentListDesc :many
SELECT id, internal_id, tariff_id, decision_id, woz_object_id, 
    owner_type, owner_id, owner_label,
    owner_address_id, owner_address_line_1, owner_address_line_2,
    woz_address_id, woz_address_line_1, woz_address_line_2,
    woz_amount, registered_people, created_at, effective_at
FROM digilab_demo_fbs.assessments    
WHERE assessments.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_fbs.assessments as sub_qry
    WHERE sub_qry.id=$1
), $2)
ORDER BY assessments.internal_id DESC
LIMIT $3;

-- name: AssessmentGet :one
SELECT id, internal_id, tariff_id, decision_id, woz_object_id, 
    owner_type, owner_id, owner_label,
    owner_address_id, owner_address_line_1, owner_address_line_2,
    woz_address_id, woz_address_line_1, woz_address_line_2,
    woz_amount, registered_people, created_at, effective_at
FROM digilab_demo_fbs.assessments
WHERE id=$1;

-- name: AssessmentGetOnTaxBill :many
SELECT id, internal_id, tariff_id, decision_id, woz_object_id, 
    owner_type, owner_id, owner_label,
    owner_address_id, owner_address_line_1, owner_address_line_2,
    woz_address_id, woz_address_line_1, woz_address_line_2,
    woz_amount, registered_people, created_at, effective_at
FROM digilab_demo_fbs.assessments
LEFT JOIN digilab_demo_fbs.tax_bill_assessment
    ON tax_bill_assessment.assessment_id = assessments.id
WHERE tax_bill_assessment.tax_bill_id=$1;

-- name: AssessmentCreate :exec
INSERT INTO digilab_demo_fbs.assessments
(id, tariff_id, decision_id, woz_object_id, 
    owner_type, owner_id, owner_label,
    owner_address_id, owner_address_line_1, owner_address_line_2,
    woz_address_id, woz_address_line_1, woz_address_line_2,
    woz_amount, registered_people, created_at, effective_at) 
VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8, $9, 
    $10, $11, $12, $13, $14, $15, $16, $17
);

-- name: AssessmentDelete :exec
DELETE FROM digilab_demo_fbs.assessments
WHERE id=$1;   

-- name: AssessmentGetOwnersBasedOnRun :many
SELECT owner_id, owner_type, owner_label 
FROM digilab_demo_fbs.runs
LEFT JOIN digilab_demo_fbs.tariffs 
    ON tariffs.run_id=runs.id
RIGHT JOIN digilab_demo_fbs.assessments
    ON assessments.tariff_id=tariffs.id
WHERE runs.id=$1
GROUP BY owner_id, owner_type, owner_label;

-- name: AssessmentsListOwner :many
SELECT assessments.id, assessments.internal_id, tariff_id, decision_id, woz_object_id, 
        owner_type, owner_id, owner_label,
        owner_address_id, owner_address_line_1, owner_address_line_2,
        woz_address_id, woz_address_line_1, woz_address_line_2,
        woz_amount, registered_people, assessments.created_at, effective_at
FROM digilab_demo_fbs.assessments
LEFT JOIN digilab_demo_fbs.runs
    ON runs.id=$2
LEFT JOIN digilab_demo_fbs.tariffs 
    ON tariffs.run_id=runs.id
WHERE owner_id=$1 AND assessments.tariff_id=tariffs.id;

-- name: AssessmentsListOwnerAll :many
SELECT assessments.id, assessments.internal_id, tariff_id, decision_id, woz_object_id, 
        assessments.owner_type, assessments.owner_id, assessments.owner_label,
        owner_address_id, owner_address_line_1, owner_address_line_2,
        woz_address_id, woz_address_line_1, woz_address_line_2,
        woz_amount, registered_people, assessments.created_at, effective_at
FROM digilab_demo_fbs.assessments
LEFT JOIN digilab_demo_fbs.tax_bills
    ON tax_bills.owner_id=$1
LEFT JOIN digilab_demo_fbs.tax_bill_assessment
    ON tax_bill_assessment.assessment_id=assessments.id;

-- name: TaxBillsCreate :exec
INSERT INTO digilab_demo_fbs.tax_bills 
(id, run_id, owner_type, owner_id, owner_label)
VALUES (
    $1, $2, $3, $4, $5
);

-- name: TaxBillAssessment :exec
INSERT INTO digilab_demo_fbs.tax_bill_assessment
(tax_bill_id, assessment_id)
VALUES ($1, $2);
