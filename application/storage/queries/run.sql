-- name: RunListAsc :many
SELECT objs.* FROM (
    SELECT id, internal_id, year, created_at, status
    FROM digilab_demo_fbs.runs    
    WHERE runs.internal_id > (
        SELECT sub_qry.internal_id
        FROM digilab_demo_fbs.runs as sub_qry
        WHERE sub_qry.id=$1
    )
    ORDER BY runs.internal_id ASC
    LIMIT $2
) objs ORDER BY objs.internal_id DESC;

-- name: RunListDesc :many
SELECT id, internal_id, year, created_at, status
FROM digilab_demo_fbs.runs    
WHERE runs.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_fbs.runs as sub_qry
    WHERE sub_qry.id=$1
), $2)
ORDER BY runs.internal_id DESC
LIMIT $3;

-- name: RunGet :one
SELECT id, internal_id, year, created_at, status
FROM digilab_demo_fbs.runs
WHERE id=$1;

-- name: RunCreate :exec
INSERT INTO digilab_demo_fbs.runs
(id, year, status, created_at)
VALUES (
    $1, $2, $3, NOW()
);

-- name: RunUpdate :exec
UPDATE digilab_demo_fbs.runs
SET status=$1
WHERE id=$2;