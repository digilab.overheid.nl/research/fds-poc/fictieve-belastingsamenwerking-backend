-- name: TariffListRun :many
SELECT id, run_id, municipality, property_tax_owner_residential, property_tax_owner_non_residential,
    property_tax_usage_non_residential, sewerage_tax, waste_tax_one_person_household, 
    waste_tax_multi_person_household
FROM digilab_demo_fbs.tariffs
WHERE run_id=$1;

-- name: TariffList :many
SELECT id, run_id, municipality, property_tax_owner_residential, property_tax_owner_non_residential,
    property_tax_usage_non_residential, sewerage_tax, waste_tax_one_person_household, 
    waste_tax_multi_person_household
FROM digilab_demo_fbs.tariffs;

-- name: TariffGet :one
SELECT id, run_id, municipality, property_tax_owner_residential, property_tax_owner_non_residential,
    property_tax_usage_non_residential, sewerage_tax, waste_tax_one_person_household, 
    waste_tax_multi_person_household
FROM digilab_demo_fbs.tariffs
WHERE id=$1;

-- name: TariffCreate :exec
INSERT INTO digilab_demo_fbs.tariffs
(id, run_id, municipality, property_tax_owner_residential, property_tax_owner_non_residential,
    property_tax_usage_non_residential, sewerage_tax, waste_tax_one_person_household, 
    waste_tax_multi_person_household)
VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8, $9
);