-- name: TaxBillListAsc :many
SELECT objs.* FROM (
    SELECT id, internal_id, run_id, owner_type, owner_id, owner_label, created_at
    FROM digilab_demo_fbs.tax_bills
    WHERE tax_bills.internal_id > (
        SELECT sub_qry.internal_id
        FROM digilab_demo_fbs.tax_bills as sub_qry
        WHERE sub_qry.id=$1
    )
    ORDER BY tax_bills.internal_id ASC
    LIMIT $2
) objs ORDER BY objs.internal_id DESC;

-- name: TaxBillListDesc :many
SELECT id, internal_id, run_id, owner_type, owner_id, owner_label, created_at
FROM digilab_demo_fbs.tax_bills
WHERE tax_bills.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_fbs.tax_bills as sub_qry
    WHERE sub_qry.id=$1
), $2)
ORDER BY tax_bills.internal_id DESC
LIMIT $3;

-- name: OwnerTaxBillListDesc :many
SELECT id, internal_id, run_id, owner_type, owner_id, owner_label, created_at
FROM digilab_demo_fbs.tax_bills
ORDER BY tax_bills.internal_id DESC;

-- name: TaxBillOwner :many
SELECT id, internal_id, run_id, owner_type, owner_id, owner_label, created_at
FROM digilab_demo_fbs.tax_bills
WHERE tax_bills.owner_id = $1
AND tax_bills.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_fbs.tax_bills as sub_qry
    WHERE sub_qry.id=$2
), $3)
ORDER BY tax_bills.internal_id DESC
LIMIT $4;

-- name: TaxBillGet :one
SELECT id, internal_id, run_id, owner_type, owner_id, owner_label, created_at
FROM digilab_demo_fbs.tax_bills
WHERE id=$1;
