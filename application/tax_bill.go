package application

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/signintech/gopdf"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/adapter"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/meta"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/pagination"
	"golang.org/x/sync/semaphore"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
)

type Owner struct {
	ID    uuid.UUID
	Type  string
	Label string
}

var printer = message.NewPrinter(language.Dutch)

func (app *Application) FinalizeTaxBill(w http.ResponseWriter, r *http.Request) {
	runID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, fmt.Errorf("cannot parse id: %w", err))
		return
	}

	app.finalizeRunStatus.Write(runID, Work{})

	go func() {
		if err := app.finalizeRun(context.Background(), runID); err != nil {
			fmt.Print(fmt.Errorf("finalize run failed: %w", err))
		}
	}()

	w.WriteHeader(http.StatusCreated)
}

func (app *Application) finalizeRun(ctx context.Context, runID uuid.UUID) error {
	const ErrSize = 100

	var (
		maxWorkers = runtime.GOMAXPROCS(0)
		sem        = semaphore.NewWeighted(int64(maxWorkers))
		errs       = make(chan error, ErrSize)
		wg         = sync.WaitGroup{}
	)

	if err := app.db.Queries.RunUpdate(ctx, &queries.RunUpdateParams{Status: int32(model.RunStatusFinalizeRunning), ID: runID}); err != nil {
		return fmt.Errorf("run update: %w", err)
	}

	records, err := app.db.Queries.AssessmentGetOwnersBasedOnRun(ctx, runID)
	if err != nil {
		return fmt.Errorf("assessment get owners based on run: %w", err)
	}

	if err := app.finalizeRunStatus.IncreaseAmount(runID, len(records)); err != nil {
		return fmt.Errorf("increase amount: %w", err)
	}

	go func() {
		for err := range errs {
			if err := app.finalizeRunStatus.IncreaseError(runID, 1); err != nil {
				fmt.Printf("increase error: %v", err)
			}

			fmt.Printf("err: %+v", err)
		}
	}()

	for idx := range records {
		if err := sem.Acquire(ctx, 1); err != nil {
			log.Printf("Failed to acquire semaphore: %v", err)
			break
		}

		wg.Add(1)

		go func(idx int) {
			defer sem.Release(1)

			if err := app.finalizeWork(ctx, runID, Owner{
				ID:    records[idx].OwnerID,
				Type:  records[idx].OwnerType,
				Label: records[idx].OwnerLabel,
			}); err != nil {
				errs <- err
			}

			wg.Done()
		}(idx)
	}

	wg.Wait()

	if err := app.finalizeRunStatus.SetFinished(runID, true); err != nil {
		return fmt.Errorf("set finished: %w", err)
	}

	if err := app.db.Queries.RunUpdate(ctx, &queries.RunUpdateParams{Status: int32(model.RunStatusFinalizeDone), ID: runID}); err != nil {
		return fmt.Errorf("run update: %w", err)
	}

	return nil
}

func (app *Application) finalizeWork(ctx context.Context, runID uuid.UUID, owner Owner) error {
	taxBillID := uuid.New()

	if err := app.db.Queries.TaxBillsCreate(ctx, &queries.TaxBillsCreateParams{
		ID:         taxBillID,
		RunID:      runID,
		OwnerType:  owner.Type,
		OwnerID:    owner.ID,
		OwnerLabel: owner.Label,
	}); err != nil {
		return fmt.Errorf("tax bill create: %w", err)
	}

	records, err := app.db.Queries.AssessmentsListOwner(ctx, &queries.AssessmentsListOwnerParams{
		OwnerID: owner.ID,
		ID:      runID,
	})
	if err != nil {
		return fmt.Errorf("assessments list owner: %w", err)
	}

	for idx := range records {
		if err := app.db.Queries.TaxBillAssessment(ctx, &queries.TaxBillAssessmentParams{
			TaxBillID:    taxBillID,
			AssessmentID: records[idx].ID,
		}); err != nil {
			return fmt.Errorf("tax bill assessment: %w", err)
		}
	}

	if err := app.finalizeRunStatus.IncreaseDone(runID, 1); err != nil {
		return fmt.Errorf("increase done: %w", err)
	}

	return nil
}

func (app *Application) TaxBillsList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*queries.DigilabDemoFbsTaxBill

	switch md.IsASC() {
	case true:
		records, err = app.taxBillsListAsc(r.Context(), md)
	case false:
		records, err = app.taxBillsListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	objects := adapter.ToTaxBills(records)

	response, err := pagination.Build(objects, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) taxBillsListAsc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFbsTaxBill, error) {
	params := &queries.TaxBillListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.TaxBillListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("tax bill list failed: %w", err)
	}

	return records, nil
}

func (app *Application) taxBillsListDesc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFbsTaxBill, error) {
	params := &queries.TaxBillListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.TaxBillListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("tax bill list failed: %w", err)
	}

	return records, nil
}

func (app *Application) OwnerTaxBillsList(w http.ResponseWriter, r *http.Request) {
	ownerID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	records, err := app.ownerTaxBillsListDesc(r.Context(), ownerID, md) // Order by newest assessments first
	if err != nil {
		writeError(w, err)
		return
	}

	objects := adapter.ToTaxBills(records)

	response, err := pagination.Build(objects, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) TaxBillGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	record, err := app.db.Queries.TaxBillGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	taxBill := adapter.ToTaxBill(record)

	if err := json.NewEncoder(w).Encode(taxBill); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) ownerTaxBillsListDesc(ctx context.Context, ownerID uuid.UUID, md *meta.Data) ([]*generated.DigilabDemoFbsTaxBill, error) {
	params := &generated.TaxBillOwnerParams{
		OwnerID:    ownerID,
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := app.db.Queries.TaxBillOwner(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("woz-object list failed: %w", err)
	}

	return records, nil
}

func (app *Application) TaxBillsStatus(w http.ResponseWriter, r *http.Request) {
	runID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	work, ok := app.finalizeRunStatus.Read(runID)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err := json.NewEncoder(w).Encode(work); err != nil {
		writeError(w, fmt.Errorf("encode failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

type couple struct {
	assessments *model.Assessment
	wozObject   *model.WOZObject
	ownerID     *uuid.UUID
	occupantID  *uuid.UUID
	tariff      *model.Tariff
}

func (app *Application) TaxBillsDownload(w http.ResponseWriter, r *http.Request) {
	taxBillID, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, fmt.Errorf("uuid parse: %w", err))
		return
	}

	// Fetch the OZB assessment
	record, err := app.db.Queries.TaxBillGet(r.Context(), taxBillID)
	if err != nil {
		writeError(w, fmt.Errorf("tax bill get: %w", err))
		return
	}

	taxBill := adapter.ToTaxBill(record)

	records, err := app.db.Queries.AssessmentGetOnTaxBill(r.Context(), taxBillID)
	if err != nil {
		writeError(w, fmt.Errorf("assessment get based on tax bill: %w", err))
		return
	}

	assessments := adapter.ToAssessments(records)

	tariffsRecord, err := app.db.Queries.TariffListRun(r.Context(), record.RunID)
	if err != nil {
		writeError(w, fmt.Errorf("tariff get: %w", err))
		return
	}

	tariffs := adapter.ToTariffs(tariffsRecord)

	wozObjects := make([]couple, 0, len(assessments))

	verwerking := model.VwlLog{
		VerwerkingsActiviteit: "http://example.com/activity/v0/woz-ozb-b",
		VerwerkingsSpan:       uuid.New(),
	}

	for idx := range assessments {
		tariff, ok := GetTariff(tariffs, assessments[idx].TariffID)
		if !ok {
			writeError(w, fmt.Errorf("get tariff failed"))
			return
		}

		api := app.municipalities[tariff.Municipality]

		wozObject := new(model.WOZObject)

		url := fmt.Sprintf("/woz-objects/%s", assessments[idx].WOZObjectID)
		if err := api.WOZ.Request(r.Context(), &verwerking, http.MethodGet, url, nil, wozObject); err != nil {
			writeError(w, fmt.Errorf("woz request: %w", err))
			return
		}

		var ownerID *uuid.UUID
		if wozObject.StakeholderOwner != nil {
			ownerID, _, _, err = app.GetStakeholder(r.Context(), &verwerking, &api, *wozObject.StakeholderOwner)
			if err != nil {
				writeError(w, fmt.Errorf("get stakeholder owner failed; %w", err))
				return
			}
		}

		var occupantID *uuid.UUID
		if wozObject.StakeholderOccupant != nil {
			occupantID, _, _, err = app.GetStakeholder(r.Context(), &verwerking, &api, *wozObject.StakeholderOccupant)
			if err != nil {
				writeError(w, fmt.Errorf("get stakeholder occupant failed: %w", err))
				return
			}
		}

		wozObjects = append(wozObjects, couple{
			assessments: &assessments[idx],
			wozObject:   wozObject,
			ownerID:     ownerID,
			occupantID:  occupantID,
		})
	}

	pdf, err := ozbPDFBuild(taxBill, tariffs, wozObjects)
	if err != nil {
		writeError(w, fmt.Errorf("ozb pdf build failed: %w", err))
		return
	}

	name := fmt.Sprintf("aanslagbiljet-%d-%s.pdf", record.CreatedAt.Year(), time.Now().Format("2006-01-02T15-04-05"))
	w.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename: %s", name))

	if _, err := pdf.WriteTo(w); err != nil {
		writeError(w, fmt.Errorf("pdf output: %w", err))
		return
	}
}

func GetTariff(tariffs []model.Tariff, id uuid.UUID) (*model.Tariff, bool) {
	for idx := range tariffs {
		if tariffs[idx].ID == id {
			return &tariffs[idx], true
		}
	}

	return nil, false
}

//nolint:gomnd,funlen // unnecessary
func ozbPDFBuild(taxBill *model.TaxBill, tariffs []model.Tariff, wozObjects []couple) (*gopdf.GoPdf, error) {
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{
		Unit:     gopdf.UnitMM,
		PageSize: *gopdf.PageSizeA4,
	})
	pdf.AddPage()

	// Load the font. Note: using separate ttf files for regular and bold, since PDF apparently does not support variable fonts yet
	font := "Inter"

	if err := pdf.AddTTFFontWithOption(font, "./fonts/inter-v13-latin_latin-ext-regular.ttf", gopdf.TtfOption{Style: gopdf.Regular}); err != nil {
		return nil, err
	}

	if err := pdf.AddTTFFontWithOption(font, "./fonts/inter-v13-latin_latin-ext-700.ttf", gopdf.TtfOption{Style: gopdf.Bold}); err != nil {
		return nil, err
	}

	pdf.SetInfo(gopdf.PdfInfo{
		Title: "Aanslagbiljet",
		// Author: ,
	})

	pdf.SetFontWithStyle(font, gopdf.Bold, 12)
	pdf.SetXY(10, 10)
	pdf.Cell(nil, "AANSLAGBILJET")

	// pdf.ImportPage("assets/template.pdf", 1, "/MediaBox") // Does not work

	// Add the logo. IMPROVE: use SVG or PDF image
	err := pdf.Image("assets/logo.png", 59, 11, &gopdf.Rect{W: 47.434, H: 27.590})
	if err != nil {
		return nil, err
	}

	pdf.SetFontWithStyle(font, gopdf.Bold, 10)
	pdf.SetXY(120, 10)
	pdf.Cell(nil, "Kenmerk")
	pdf.SetXY(120, 16)
	pdf.Cell(nil, "Belastingjaar")
	pdf.SetXY(120, 22)
	pdf.Cell(nil, "Dagtekening")

	pdf.SetFontWithStyle(font, gopdf.Regular, 10)

	pdf.SetXY(150, 10)
	pdf.Cell(nil, base64.RawURLEncoding.EncodeToString(taxBill.ID[:]))
	pdf.SetXY(150, 16)
	pdf.Cell(nil, fmt.Sprintf("%d", taxBill.CreatedAt.Year()))
	pdf.SetXY(150, 22)
	pdf.Cell(nil, taxBill.CreatedAt.Format("02 Jan 2006"))

	pdf.SetXY(10, 50)
	pdf.Cell(nil, taxBill.OwnerLabel)
	pdf.SetXY(10, 55)
	pdf.Cell(nil, wozObjects[0].assessments.OwnerAddressLine1)
	pdf.SetXY(10, 60)
	pdf.Cell(nil, wozObjects[0].assessments.OwnerAddressLine2)

	pdf.SetFontWithStyle(font, gopdf.Bold, 10)

	col1, col2, col3, col4, col5, col6 := 10., 40., 60., 115., 150., 170.

	y := 80.0
	pdf.SetY(y)

	pdf.SetX(col1)
	pdf.Cell(nil, "Soort belasting")

	pdf.SetX(col2)
	pdf.Cell(nil, "Periode")

	pdf.SetX(col3)
	pdf.Cell(nil, "Omschrijving belastingobject")

	pdf.SetX(col4)
	pdf.Cell(nil, "Heffingsmaatstaf")

	pdf.SetX(col5)
	pdf.Cell(nil, "Tarief")

	pdf.SetX(col6)
	pdf.Cell(nil, "Bedrag")

	y += 8
	drawLine(&pdf, 10, y, 200, y)

	previousYear := taxBill.CreatedAt.AddDate(-1, 0, 0)
	totalAmount := 0.0

	pdf.SetFontWithStyle(font, gopdf.Regular, 10)

	y += 6

	for _, object := range wozObjects {
		assessment := object.assessments
		wozObject := object.wozObject

		tariff, ok := GetTariff(tariffs, assessment.TariffID)
		if !ok {
			return nil, fmt.Errorf("tariff cannot be found")
		}

		var wasteCharges float64
		if assessment.RegisteredPeople > 1 {
			wasteCharges = tariff.WasteTaxMultiPersonHousehold
		} else {
			wasteCharges = tariff.WasteTaxOnePersonHousehold
		}

		propertyTax := 0.0

		if object.ownerID != nil && *object.ownerID == assessment.OwnerID {
			if wozObject.Type == model.Residential {
				propertyTax = tariff.PropertyTaxOwnershipResidential
			} else if wozObject.Type == model.NonResidential {
				propertyTax = tariff.PropertyTaxOwnershipNonResidential
			}
		} else if object.occupantID != nil && *object.occupantID == assessment.OwnerID {
			if wozObject.Type == model.NonResidential {
				propertyTax = tariff.PropertyTaxUsageNonResidential
			}
		}

		ozbTaxAmount := math.Round(float64(assessment.WOZAmount)*propertyTax) / 100
		totalAmount += math.Round((ozbTaxAmount+tariff.SewerageTax+wasteCharges)*100) / 100

		pdf.SetFont(font, "", 8)
		pdf.SetY(y)

		if propertyTax != 0.0 {
			pdf.SetX(col1)
			pdf.Cell(nil, "WOZ-beschikking")
			pdf.SetX(col2)
			pdf.Cell(nil, fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()))
			pdf.SetX(col3)
			pdf.Cell(nil, strings.Replace(strings.Replace(assessment.WOZAddressLine1, ", ", "\n", 1), ",", "", 1))
			pdf.SetXY(col4, y-3)
			pdf.Cell(nil, fmt.Sprintf("WOZ-waarde: € %d", assessment.WOZAmount))
			pdf.SetXY(col4, y+3)
			pdf.Cell(nil, fmt.Sprintf("Waardepeildatum: 01-01-%d", previousYear.Year()))

			drawLine(&pdf, 10, y+8, 200, y+8)

			y += 15
		}

		if propertyTax != 0.0 {
			pdf.SetY(y)
			pdf.SetX(col1)
			pdf.Cell(nil, "OZB-eigenaar")
			pdf.SetX(col2)
			pdf.Cell(nil, fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()))
			pdf.SetX(col3)
			pdf.Cell(nil, strings.Replace(strings.Replace(assessment.WOZAddressLine1, ", ", "\n", 1), ",", "", 1))
			pdf.SetX(col4)
			pdf.Cell(nil, fmt.Sprintf("WOZ-waarde: € %d", assessment.WOZAmount))
			pdf.SetX(col5)
			pdf.Cell(nil, fmt.Sprintf("%.6f %%", propertyTax))
			pdf.SetX(col6)
			pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(ozbTaxAmount)))

			drawLine(&pdf, 10, y+8, 200, y+8)

			y += 15
		}

		pdf.SetY(y)

		pdf.SetX(col1)
		pdf.Cell(nil, "Rioolheffing eigenaar")
		pdf.SetX(col2)
		pdf.Cell(nil, fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()))
		pdf.SetX(col3)
		pdf.Cell(nil, strings.Replace(strings.Replace(assessment.WOZAddressLine1, ", ", "\n", 1), ",", "", 1))
		pdf.SetX(col4)
		pdf.Cell(nil, "Aantal aanslagen: 1")
		pdf.SetX(col5)
		pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(tariff.SewerageTax)))
		pdf.SetX(col6)
		pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(tariff.SewerageTax)))

		drawLine(&pdf, 10, y+8, 200, y+8)

		y += 15
		pdf.SetY(y)

		pdf.SetX(col1)
		pdf.Cell(nil, "Afvalstoffenheffing")
		pdf.SetX(col2)
		pdf.Cell(nil, fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()))
		pdf.SetX(col3)
		pdf.Cell(nil, strings.Replace(strings.Replace(assessment.WOZAddressLine1, ", ", "\n", 1), ",", "", 1))
		pdf.SetX(col4)
		household := "Eenpersoonshuishouden"
		if assessment.RegisteredPeople > 1 {
			household = "Meerpersoonshuishouden"
		}
		pdf.Cell(nil, household)
		pdf.SetX(col5)
		pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(wasteCharges)))
		pdf.SetX(col6)
		pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(wasteCharges)))

		drawLine(&pdf, 10, y+8, 200, y+8)
		y += 15
	}

	pdf.SetXY(150, y)
	pdf.SetFontWithStyle(font, gopdf.Bold, 10)
	pdf.Cell(nil, "Totaal: ")
	pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(totalAmount)))

	monthlyAmount := math.Round(totalAmount*10) / 100
	pdf.SetFontWithStyle(font, gopdf.Regular, 10)
	y += 20
	pdf.SetXY(10, y)
	pdf.Cell(nil, "Het aanslagbedraq wordt in 10 gelijke bedragen maandelijks afgeschreven rond de 25e van elke maand.")
	pdf.SetXY(10, y+5)
	pdf.Cell(nil, "Het termijnbedrag is ")
	pdf.SetFontWithStyle(font, gopdf.Bold, 10)
	pdf.Cell(nil, fmt.Sprintf("€ %s", numberFormat(monthlyAmount)))
	pdf.SetFontWithStyle(font, gopdf.Regular, 10)
	pdf.Cell(nil, " en wordt afgeschreven van rekeningnummer ")
	pdf.SetFontWithStyle(font, gopdf.Bold, 10)
	pdf.Cell(nil, "NL02ABNA0123456789")
	pdf.SetFontWithStyle(font, gopdf.Regular, 10)
	pdf.Cell(nil, ".")
	pdf.SetXY(10, y+10)
	pdf.Cell(nil, "De 1e incassotermijn valt in de eerste maand na dagtekening van de aanslag.")

	return &pdf, nil
}

//nolint:gomnd // fixed values
func drawLine(pdf *gopdf.GoPdf, x1, y1, x2, y2 float64) {
	// pdf.SetLineType()
	pdf.SetStrokeColor(0xe5, 0xe7, 0xeb)
	pdf.SetLineWidth(.2)
	pdf.Line(x1, y1, x2, y2)
}

func numberFormat(v interface{}) string {
	if v == nil {
		return ""
	}

	switch v.(type) {
	case int, int8, int16, int32, int64, uint, uint16, uint32, uint64:
		// Integer: show the number without decimals
		return printer.Sprint(v)
	default:
		// Otherwise (e.g. float): assume the number is a currency, so show the number with two fixed digits
		return printer.Sprint(number.Decimal(v, number.MinFractionDigits(2), number.MaxFractionDigits(2)))
	}

	// IMPROVE: use a library like github.com/shopspring/decimal for decimal numbers instead of float64, see https://github.com/shopspring/decimal#why-dont-you-just-use-float64
}
