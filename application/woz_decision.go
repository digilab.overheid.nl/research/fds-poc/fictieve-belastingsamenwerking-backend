package application

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage/adapter"
)

func (app *Application) DecisionBasedOnWozObject(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, fmt.Errorf("cannot parse uuid: %w", err))
		return
	}

	records, err := app.db.Queries.DecisionsBasedOnWozObject(r.Context(), id)
	if err != nil {
		writeError(w, fmt.Errorf("decision list object id: %w", err))
		return
	}

	items := make([]model.WozDecision, 0, len(records))
	for idx := range records {
		items = append(items, *adapter.ToDecision(records[idx]))
	}

	if err := json.NewEncoder(w).Encode(items); err != nil {
		writeError(w, fmt.Errorf("encoding failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}
