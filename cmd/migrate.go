package cmd

import (
	"context"
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/config"
)

var migrateOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	migrateInitCommand.Flags().StringVarP(&migrateOpts.ConfigPath, "config-path", "", "", "Location of the config")
	migrateUpCommand.Flags().StringVarP(&migrateOpts.ConfigPath, "config-path", "", "", "Location of the config")

	if err := migrateInitCommand.MarkFlagRequired("config-path"); err != nil {
		log.Fatal("config path is required")
	}

	if err := migrateUpCommand.MarkFlagRequired("config-path"); err != nil {
		log.Fatal("config path is required")
	}

	migrateCommand.AddCommand(migrateUpCommand)
	migrateCommand.AddCommand(migrateInitCommand)
}

var migrateCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "migrate",
	Short: "Run the migration tool",
	Run:   func(cmd *cobra.Command, args []string) {},
}

var migrateUpCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "up",
	Short: "Migrate the API",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.New(migrateOpts.ConfigPath)
		if err != nil {
			log.Fatalf(fmt.Errorf("failed to create new config: %w", err).Error())
		}

		if err := storage.PostgresPerformMigrations(cfg.PostgresDSN, PSQLSchemaName); err != nil {
			log.Fatalf(fmt.Errorf("failed to perform migrations: %w", err).Error())
		}

		fmt.Println("Migrate up successful")
	},
}

var migrateInitCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "init",
	Short: "Init the API",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.New(migrateOpts.ConfigPath)
		if err != nil {
			log.Fatal(fmt.Errorf("failed to create new config: %w", err).Error())
		}

		if err := migrateInit(cfg.PostgresDSN); err != nil {
			log.Fatal(fmt.Errorf("migrate init: %w", err))
		}
	},
}

func migrateInit(dns string) error {
	db, err := storage.NewPostgreSQLConnection(dns)
	if err != nil {
		return fmt.Errorf("failed to create new config: %w", err)
	}

	if _, err := db.ExecContext(context.Background(), fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", PSQLSchemaName)); err != nil {
		return fmt.Errorf("failed to create schema: %w", err)
	}

	fmt.Println("migrate init successful")

	return nil
}
