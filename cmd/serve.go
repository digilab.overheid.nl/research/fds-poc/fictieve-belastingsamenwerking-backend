package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/rc"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	serveCommand.Flags().StringVarP(&serveOpts.ConfigPath, "config-path", "", "./config/config.yaml", "Location of the config")

	if err := serveCommand.MarkFlagRequired("config-path"); err != nil {
		log.Fatal("config location is required")
	}
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.New(serveOpts.ConfigPath)
		if err != nil {
			log.Fatal(fmt.Errorf("failed to create new config: %w", err))
		}

		if err := migrateInit(cfg.PostgresDSN); err != nil {
			log.Fatal(fmt.Errorf("failed to migrate init: %w", err))
		}

		if err := storage.PostgresPerformMigrations(cfg.PostgresDSN, PSQLSchemaName); err != nil {
			log.Fatal(fmt.Errorf("failed to migrate db: %w", err))
		}

		db, err := storage.New(cfg.PostgresDSN)
		if err != nil {
			log.Fatal(fmt.Errorf("failed to connect to the database: %w", err))
		}

		municipalities := make(map[string]application.Municipality)

		vwlService := cfg.VwlService

		lvFrp := rc.New("lv-frp", cfg.LVFRP.Domain, cfg.LVFRP.FSCGrantHash, model.VwlConfig{
			VwlService:        vwlService,
			Systeem:           "fictieve-belastingsamenwerking-backend",
			Verantwoordelijke: "belastingsamenwerking",
			Verwerker:         "belastingsamenwerking",
		})

		for _, v := range cfg.Municipalities {

			vwlConfig := model.VwlConfig{
				VwlService:        vwlService,
				Systeem:           "fictieve-belastingsamenwerking-backend",
				Verantwoordelijke: v.ID,
				Verwerker:         "belastingsamenwerking",
			}

			municipalities[strings.ToLower(v.ID)] = application.Municipality{
				FRP: rc.NewFRP(
					rc.New("frp", v.FRP.Domain, v.FRP.FSCGrantHash, vwlConfig),
					lvFrp),
				WOZ: rc.New("woz", v.WOZ.Domain, v.WOZ.FSCGrantHash, vwlConfig),
			}
		}

		vwlConfig := model.VwlConfig{
			VwlService:        vwlService,
			Systeem:           "fictieve-belastingsamenwerking-backend",
			Verantwoordelijke: "belastingsamenwerking",
			Verwerker:         "belastingsamenwerking",
		}

		frag := rc.New("frag", cfg.FRAG.Domain, cfg.FRAG.FSCGrantHash, vwlConfig)
		app := application.New(cfg.BackendListenAddress, municipalities, frag, db)

		app.Router()

		if err := app.ListenAndServe(); err != nil {
			log.Fatal(err)
		}

		os.Exit(0)
	},
}
