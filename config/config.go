package config

import (
	"path/filepath"
	"strings"

	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
)

type Config struct {
	PostgresDSN          string `mapstructure:"postgres_dsn"`
	BackendListenAddress string
	Municipalities       []Municipality
	FRAG                 Domain
	LVFRP                Domain
	VwlService           string
}

type Municipality struct {
	ID  string
	FRP Domain
	WOZ Domain
}

type Domain struct {
	Domain       string
	FSCGrantHash string
}

func New(configPath string) (*Config, error) {
	path := filepath.Dir(configPath)
	base := filepath.Base(configPath)
	ext := filepath.Ext(base)
	file, _ := strings.CutSuffix(base, ext)

	e := enviper.New(viper.New())
	e.AddConfigPath(path)
	e.SetConfigName(file)

	if err := e.ReadInConfig(); err != nil {
		panic(err)
	}

	config := new(Config)

	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}

	return config, nil
}
