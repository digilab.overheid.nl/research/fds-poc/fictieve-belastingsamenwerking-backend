package meta

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/google/uuid"
)

type Data struct {
	FirstID uuid.UUID `json:"firstId"`
	LastID  uuid.UUID `json:"lastId"`
	PerPage int       `json:"perPage"`
}

func (meta *Data) IsASC() bool {
	empty := uuid.UUID{}

	return meta.FirstID != empty && meta.LastID == empty
}

const DefaultPerPage = 10

func New(r *http.Request) (*Data, error) {
	perPage, err := GetPerPage(r)
	if err != nil {
		return nil, err
	}

	firstID, err := GetID(r, "firstId")
	if err != nil {
		return nil, err
	}

	lastID, err := GetID(r, "lastId")
	if err != nil {
		return nil, err
	}

	return &Data{
		PerPage: perPage,
		FirstID: firstID,
		LastID:  lastID,
	}, nil
}

func GetID(r *http.Request, key string) (uuid.UUID, error) {
	str := r.URL.Query().Get(key)

	id, err := uuid.Parse(str)
	if err != nil {
		return uuid.UUID{}, nil //nolint:nilerr // we dont want to return the error
	}

	return id, nil
}

func GetPerPage(r *http.Request) (int, error) {
	strAmount := r.URL.Query().Get("perPage")
	if strings.EqualFold(strAmount, "") {
		return DefaultPerPage, nil
	}

	amount, err := strconv.Atoi(strAmount)
	if err != nil {
		return 0, fmt.Errorf("parsing 'perPage' failed: %w", err)
	}

	return amount, nil
}
