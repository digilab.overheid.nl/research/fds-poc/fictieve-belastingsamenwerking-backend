package pagination

import (
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/meta"
)

type Response[T any] struct {
	Data       []T         `json:"data"`
	Pagination *Pagination `json:"pagination"`
}

type Pagination struct {
	FirstID *uuid.UUID `json:"firstId,omitempty"`
	LastID  *uuid.UUID `json:"lastId,omitempty"`
	PerPage int        `json:"perPage"`
}

type IDer interface {
	GetID() uuid.UUID
}

func New(moreThanPerPage bool, md *meta.Data, firstID, lastID uuid.UUID) *Pagination {
	empty := uuid.UUID{}
	noIDs, previousLastID, previousFirstID := false, false, false

	if md.FirstID != empty {
		previousFirstID = true
	} else if md.LastID != empty {
		previousLastID = true
	} else {
		noIDs = true
	}

	page := &Pagination{
		PerPage: md.PerPage,
	}
	isSet := false

	if (moreThanPerPage && (noIDs || previousLastID)) || previousFirstID {
		isSet = true
		page.LastID = &lastID
	}

	if (moreThanPerPage && previousFirstID) || previousLastID {
		isSet = true
		page.FirstID = &firstID
	}

	if !isSet {
		page = nil
	}

	return page
}

func Build[T IDer](slice []T, md *meta.Data) (Response[T], error) {
	moreThanPerPage := len(slice) > md.PerPage

	if moreThanPerPage {
		if md.IsASC() {
			slice = slice[1:]
		} else {
			slice = slice[:md.PerPage]
		}
	}

	var page *Pagination
	if len(slice) > 0 {
		page = New(moreThanPerPage, md, slice[0].GetID(), slice[len(slice)-1].GetID())
	}

	return Response[T]{
		Data:       slice,
		Pagination: page,
	}, nil
}
