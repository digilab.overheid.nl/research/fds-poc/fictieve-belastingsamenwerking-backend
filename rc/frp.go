package rc

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
)

type FRP struct {
	municipality *RestCall
	lv           *RestCall
}

func NewFRP(municipality, lv *RestCall) *FRP {
	return &FRP{
		municipality: municipality,
		lv:           lv,
	}
}

func (frp *FRP) GetPerson(ctx context.Context, vwl *model.VwlLog, id uuid.UUID) (*model.Person, error) {
	item := new(model.Person)
	var err error

	url := fmt.Sprintf("/people/%s", id)
	if err = frp.municipality.Request(ctx, vwl, http.MethodGet, url, nil, item); err != nil {
		return item, nil
	}

	errs := errors.Join(fmt.Errorf("get people municipality failed: %w", err))

	if err = frp.lv.Request(ctx, vwl, http.MethodGet, url, nil, item); err == nil {
		return item, nil
	}

	errs = errors.Join(errs, fmt.Errorf("get people lv failed: %w", err))

	return nil, errs
}

func (frp *FRP) GetPersonBSN(ctx context.Context, vwl *model.VwlLog, bsn string) (*model.Person, error) {
	item := new(model.Person)
	var err error

	url := fmt.Sprintf("/people/%s", bsn)
	if err = frp.municipality.Request(ctx, vwl, http.MethodGet, url, nil, item); err != nil {
		return item, nil
	}

	errs := errors.Join(fmt.Errorf("get people municipality failed: %w", err))

	if err = frp.lv.Request(ctx, vwl, http.MethodGet, url, nil, item); err == nil {
		return item, nil
	}

	errs = errors.Join(errs, fmt.Errorf("get people lv failed: %w", err))

	return nil, errs
}
