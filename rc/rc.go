package rc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"

	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-backend/application/model"
)

var ErrResponseFailed = errors.New("response failed with")

// this is package to wrap a rest call.
type RestCall struct {
	name         string
	backendURL   string
	fscGrantHash string
	client       *http.Client
	vwlConfig    model.VwlConfig
}

func New(name, backendURL, fscGrantHash string, vwlConfig model.VwlConfig) *RestCall {
	return &RestCall{
		name:         name,
		backendURL:   backendURL,
		fscGrantHash: fscGrantHash,
		client:       http.DefaultClient,
		vwlConfig:    vwlConfig,
	}
}

func (rc *RestCall) Request(ctx context.Context, verwerking *model.VwlLog, method, path string, data, value any) error {
	buf := new(bytes.Buffer)
	if data != nil {
		if err := json.NewEncoder(buf).Encode(data); err != nil {
			return fmt.Errorf("encoding failed: %w", err)
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, rc.backendURL+path, buf)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	if rc.fscGrantHash != "" {
		req.Header.Set("Fsc-Grant-Hash", rc.fscGrantHash)
	}

	if verwerking != nil && rc.vwlConfig.VwlService == "" {
		req.Header.Set("FSC-VWL-systeem", rc.vwlConfig.Systeem)
		req.Header.Set("FSC-VWL-verantwoordelijke", rc.vwlConfig.Verantwoordelijke)
		req.Header.Set("FSC-VWL-verwerker", rc.vwlConfig.Verwerker)
		req.Header.Set("FSC-VWL-verwerkings-activiteit", verwerking.VerwerkingsActiviteit)
		req.Header.Set("FSC-VWL-verwerkings-span", verwerking.VerwerkingsSpan.String())
	}

	if verwerking != nil && rc.vwlConfig.VwlService != "" {
		message := model.VwlLogMessage{
			VerwerkingsActiviteit: verwerking.VerwerkingsActiviteit,
			VerwerkingsSpan:       verwerking.VerwerkingsSpan,
			Systeem:               rc.vwlConfig.Systeem,
			Verantwoordelijke:     rc.vwlConfig.Verantwoordelijke,
			Verwerker:             rc.vwlConfig.Verwerker,
		}
		sendVerwerking(&ctx, rc.vwlConfig.VwlService, message)
	}

	resp, err := rc.client.Do(req)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		req, _ := httputil.DumpRequestOut(req, true)
		res, _ := httputil.DumpResponse(resp, true)
		return fmt.Errorf("%w\nrequest: %s\nresponse: %s", ErrResponseFailed, string(req), string(res))
	}

	if value != nil {
		if err := json.NewDecoder(resp.Body).Decode(value); err != nil {
			return fmt.Errorf("decoding failed: %w", err)
		}
	}

	return nil
}

func sendVerwerking(ctx *context.Context, vwlService string, message model.VwlLogMessage) (*model.VwlLogMessageResponse, error) {
	client := http.Client{}

	body, err := json.Marshal(message)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, vwlService, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("vwl service return non 200 status code. status code: %d", resp.StatusCode)
	}

	response := &model.VwlLogMessageResponse{}

	err = json.NewDecoder(resp.Body).Decode(response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
